# Microservices Workshop
Lab 02: Meeting the Application

---

# Tasks

 - Build the application
 
 - Meet the application
 
 - Meet the application API
  
 - Inspect the sources

---
&nbsp;

## Build the application:

&nbsp;

 - Conect to the build server using ssh:
 
```
$ ssh vsts@<SERVER-IP>
```

 - Clone the application "all-in-one" repository using the command below:
 
```
$ git clone https://github.com/selaworkshops/microservices-calculator-app-all.git
```

 - Open the docker-compose.yml file and update the variable HOST_IP in the ui-service with your own ip:
 
```
ui-service:
    build: "./ui-service"
    container_name: ui-service
    environment:
      - HOST_IP=localhost
    restart: always
    ports:
     - "3000:3000"
    networks:
     - "npm-microservices"
```
```
ui-service:
    build: "./ui-service"
    container_name: ui-service
    environment:
      - HOST_IP=<SERVER-IP>
    restart: always
    ports:
     - "3000:3000"
    networks:
     - "npm-microservices"
```

 - To build the application move to the repository root and use docker-compose:
 
```
$ cd microservices-calculator-app-all
$ docker-compose up
```

&nbsp;

## Meet the application:

&nbsp;

 - Browse to the application main page (ui-service) using the url below:
 
```
http://<SERVER-IP>:3000
```

 - Set 2 numbers and click "calculate"

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">

&nbsp;

## Meet the application API:

&nbsp;

 - Access the application UI using the url below:
 
```
http://<SERVER-IP>:3000
```

 - Access the Sum API using the url below:
 
```
http://<SERVER-IP>:3001/sum/12/13
```

 - Access the Division API using the url below:
 
```
http://<SERVER-IP>:3004/division/15/3
```

 - Access the Multiplication API using the url below:
 
```
http://<SERVER-IP>:3003/multiplication/8/3
```

 - Access the Subtraction API using the url below:
 
```
http://<SERVER-IP>:3002/subtraction/8/2
```
 
 
&nbsp;

## Inspect the sources:

 - sum-service:  
  

https://github.com/selaworkshops/sum-service


 - subtraction-service:  

https://github.com/selaworkshops/subtraction-service


 - multiplication-service:  

https://github.com/selaworkshops/multiplication-service


 - division-service:  

https://github.com/selaworkshops/division-service


 - ui-service:  

https://github.com/selaworkshops/ui-service


&nbsp;

 - Stop the docker-compose process (Ctrl+C) and exit from the build server
 

 